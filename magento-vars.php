<?php
function isBoost($host)
{
   return strpos($host, 'boost') !== false;
}

if (isset($_SERVER['HTTP_HOST'])) {
   if (isBoost($_SERVER['HTTP_HOST'])) {
      $_SERVER["MAGE_RUN_CODE"] = "boost";
      $_SERVER["MAGE_RUN_TYPE"] = "website";
   } else {
      $_SERVER["MAGE_RUN_CODE"] = "base";
      $_SERVER["MAGE_RUN_TYPE"] = "website";
   }
}
