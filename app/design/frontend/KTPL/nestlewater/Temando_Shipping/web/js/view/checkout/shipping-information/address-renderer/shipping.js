/**
 * Refer to LICENSE.txt distributed with the Temando Shipping module for notice of license
 */

define([
    'underscore',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'Temando_Shipping/js/model/collection-points',
    'Temando_Shipping/js/model/pickup-locations'
], function (_, Component, customerData, collectionPoints, pickupLocations) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-information/address-renderer/default'
        },
        collectionPoints: collectionPoints,
        pickupLocations: pickupLocations,

        /**
         * @param {*} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },

        getRegionNameByCode: function (countryId, regionCode) {
            var result = regionCode;
            var countryRegions = countryData()[countryId].regions || {};

            if (_.size(countryRegions) > 0) {
                var region = _.filter(countryRegions, (function (element) {
                        return element.code === regionCode;
                    })
                );

                if (region.length > 0) {
                    result = region[0].name;
                }
            }

            return result;
        },

        /**
         * Get customer attribute label
         *
         * @param {*} attribute
         * @returns {*}
         */
        getCustomAttributeLabel: function (attribute) {
            var resultAttribute;

            if (typeof attribute === 'string') {
                return attribute;
            }

            if (attribute.label) {
                return attribute.label;
            }

            resultAttribute = _.findWhere(this.source.get('customAttributes')[attribute['attribute_code']], {
                value: attribute.value
            });

            return resultAttribute && resultAttribute.label || attribute.value;
        },

        getTemplate: function () {
            var collectionPoint = collectionPoints.getSelectedCollectionPoint();
            if (collectionPoint) {
                return 'Temando_Shipping/checkout/shipping/address-renderer/collection-point';
            }
            var pickupLocation = pickupLocations.getSelectedPickupLocation();
            if (pickupLocation) {
                return 'Temando_Shipping/checkout/shipping/address-renderer/pickup-location';
            }
            // handle other specific adresses here
            return this.template;
        }
    });
});
