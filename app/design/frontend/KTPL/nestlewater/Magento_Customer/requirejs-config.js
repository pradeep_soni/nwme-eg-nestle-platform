var config = {
    map: {
        "*": {
            cevents: 'Magento_Customer/js/events',
            suspend: 'Magento_Customer/js/suspenddelivery',
            suspendPopup: 'Magento_Customer/js/suspenddeliverypopup',
            purify:'Magento_Customer/js/purify'
        }
    }
}
