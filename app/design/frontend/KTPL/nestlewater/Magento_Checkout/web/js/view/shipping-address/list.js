/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'ko',
    'mageUtils',
    'uiComponent',
    'uiLayout',
    'Magento_Customer/js/model/address-list',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote'
], function (_, ko, utils, Component, layout, addressList, customer, quote) {
    'use strict';
    
    var defaultRendererTemplate = {
        parent: '${ $.$data.parentName }',
        name: '${ $.$data.name }',
        component: 'Magento_Checkout/js/view/shipping-address/address-renderer/default',
        provider: 'checkoutProvider'
    };

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-address/list',
            visible: addressList().length > 0,
            rendererTemplates: []
        },

        /** @inheritdoc */
        initialize: function () {
            this._super()
                .initChildren();

            addressList.subscribe(function (changes) {
                    var self = this;

                    changes.forEach(function (change) {
                        if (change.status === 'added') {
                            self.createRendererComponent(change.value, change.index);
                        }
                    });
                },
                this,
                'arrayChange'
            );

            return this;
        },

        /** @inheritdoc */
        initConfig: function () {
            this._super();
            // the list of child components that are responsible for address rendering
            this.rendererComponents = [];

            return this;
        },

        /** @inheritdoc */
        initChildren: function () {
            _.each(addressList(), this.createRendererComponent, this);

            return this;
        },

        /**
         * Create new component that will render given address in the address list
         *
         * @param {Object} address
         * @param {*} index
         */
        createRendererComponent: function (address, index) {
            var rendererTemplate, templateData, rendererComponent;
            var itemData = quote.getItems();
            var isPet = false;
             _.each(itemData, function(element, index) {
                var sk = element.sku;
                /*if (sk.substring(0, 2) == "SN") {
                    isPet = true;
                    return false;
                }*/
            });
             
            console.log(itemData.customAttributes);

            if (index in this.rendererComponents) {
                this.rendererComponents[index].address(address);
            } else {


            //parth code add
            if(typeof(customer.customerData.custom_attributes.acw_customer_id) != "undefined"
             && customer.customerData.custom_attributes.acw_customer_id !== null) {
                if(typeof(address.customAttributes) != "undefined"
                && address.customAttributes !== null) {
                    if(isPet){
                        if(address.customAttributes.is_delivery.value == 0 || address.customAttributes.acw_customer_address_status.value == 'pending' || address.customAttributes.pet_delivery.value == 0){
                            return this;
                        }
                    } else {
                        if(address.customAttributes.is_delivery.value == 0 || address.customAttributes.acw_customer_address_status.value == 'pending'){
                            return this;
                        }
                    }
                    
                }    
            }
            //parth code add end
               
                // rendererTemplates are provided via layout
                rendererTemplate = address.getType() != undefined && this.rendererTemplates[address.getType()] != undefined ? //eslint-disable-line
                    utils.extend({}, defaultRendererTemplate, this.rendererTemplates[address.getType()]) :
                    defaultRendererTemplate;
                templateData = {
                    parentName: this.name,
                    name: index
                };
                rendererComponent = utils.template(rendererTemplate, templateData);
                utils.extend(rendererComponent, {
                    address: ko.observable(address)
                });
                layout([rendererComponent]);
                this.rendererComponents[index] = rendererComponent;
            }
        }
    });
});
