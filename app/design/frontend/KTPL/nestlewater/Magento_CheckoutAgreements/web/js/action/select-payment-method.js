/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'Magento_CheckoutAgreements/js/view/checkout-agreements'
], function ($,quote,agreementProvider) {
    'use strict';

    return function (paymentMethod) {
        if (paymentMethod) {
            paymentMethod.__disableTmpl = {
                title: true
            };
        }

        agreementProvider.call().checkboxVisible(false);
        if(paymentMethod.method == 'chcybersource')
        {
            agreementProvider.call().checkboxVisible(true);
            agreementProvider.call().checkboxTextNew('Nestlé Pure Life must confirm your address is within our coverage area, if your address is not confirmed, your money will be returned within 30 days.');
            // $(document).ready(function(){
            //     console.log($('.checkout-agreement'));
            // });
            // $('.checkout-agreement').append('<h1>tfds</h1>');
        }
        quote.paymentMethod(paymentMethod);
    };
});

